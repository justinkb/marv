# Copyright 2009 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'rb_libtorrent-0.14.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

MY_PN="libtorrent"
require github [ user=arvidn suffix=tar.gz pnv=${PNV} release="v${PV}" ]
require python [ blacklist=2 multibuild=false with_opt=true ]
# 3.17.0 only for python bindings
require cmake [ cmake_minimum_version=3.17.0 ]

SUMMARY="BitTorrent library written in C++"
DESCRIPTION="libtorrent is a C++ implementation of the BitTorrent protocol
with the goals of being efficient and easy to use."

HOMEPAGE="https://www.rasterbar.com/products/libtorrent/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    debug [[ description = [ Enable assertions ] ]]
    examples [[ description = [ Build example applications ] ]]
    logging [[ description = [ Generate extra debug logging ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/boost[>=1.54]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        python? ( dev-libs/boost[>=1.54][python][python_abis:*(-)?] )
"

# Multiple tests hangs or fail even with disabled_net (last checked: 1.2.1)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -Dboost-python-module-name="python${PYTHON_ABIS//.}"
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    "debug developer-options"
    "examples build_examples"
    "logging"
    "python python-bindings"
    "python python-egg-info"
)
CMAKE_SRC_CONFIGURE_TESTS=(
    "-Dbuild_tests=ON"
)

src_test() {
    # The tests need extensive network access to work.
    esandbox disable_net

    default
}

